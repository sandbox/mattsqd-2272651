<?php
/**
 * @file
 * Handles theme layer hooks.
 */

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_icon_font_widget(&$vars) {
  // Create an override for each version so they can have their own template.
  $vars['template_files'][] = 'icon-font-widget';

  // Preprocess this and set any value if you'd like to skip the default JS
  // from running.
  $vars['skip'] = '';

  // These are the icons that are displayed to the user.
  $vars['icons'] = theme('icon_font_widget_icons', $vars['version']);
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_icon_font_widget_display(&$vars) {
  // The version is prefixed to the class so that you know how to display it.
  $vars['version'] = substr($vars['icon'], 0, strpos($vars['icon'], '--'));

  // This is the goal of the module, to get this value to create the icon font.
  $vars['class'] = substr($vars['icon'], strpos($vars['icon'], '--') + 2);

  // Create an override for each version so they can have their own template.
  if (strlen($vars['version'])) {
    $vars['template_files'][] = 'icon-font-widget-display-' . $vars['version'];
  }

  // Preprocess this value for your given version to create a display.
  $vars['icon'] = '';
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_icon_font_widget_icons(&$vars) {
  // Create an override for each version so they can have their own template.
  if (strlen($vars['version'])) {
    $vars['template_files'][] = 'icon-font-widget-icons-' . $vars['version'];
  }

  // If you would like to provide your own icons for a specific version.
  $vars['icons'] = t('You must preprocess icon_font_widget_icons or invoke icon_font_widget_icons if you want to display a custom version.');
  $icons = module_invoke_all('icon_font_widget_icons', $vars['version']);
  if (!empty($icons)) {
    foreach ($icons as $icons_text) {
      $vars['icons'] .= $icons_text;
    }
  }
}

