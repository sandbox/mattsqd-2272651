<?php
/**
 * @file
 * Default theme implementation for rendering a font icon.
 *
 * Available variables:
 * - $version string
 *     This is the type of icon set we are using.
 * - $class string
 *     This is the class that will create the icon.
 */
?>
<?php if ($class): ?>
  <i class="<?php echo $class; ?> custom-feature-icon"></i>
<?php endif; ?>
