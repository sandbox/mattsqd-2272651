<?php
/**
 * @file
 * Default theme implementation for rendering the icon font widget icons.
 *
 * Since end users would not be able to add new templates that are picked up
 * in $vars['template_files'][] = 'icon-font-widget-icons-' . $vars['version'];
 * this is where there icons would be printed.
 *
 * Available variables:
 * - $version string
 *     This is the type of icon set we are using.
 * - $icons string
 *     This is the icons to display.
 */
print $icons;
