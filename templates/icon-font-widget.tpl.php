<?php
/**
 * @file
 * Default theme implementation for rendering the icon font widget.
 *
 * Available variables:
 * - $input_id string
 *     This is the value of the hidden field that will be saved with the node.
 * - $version string
 *     This is the type of icon set we are using.
 * - $label string
 *     This will be the label of the input.
 */
?>
<div class="icon-font-widget" data-input-id="<?php print $input_id; ?>" data-version="<?php print $version; ?>" data-skip="<?php print $skip; ?>">
  <label for="<?php print $input_id; ?>-selected-container"><?php print $label;?>:</label>
  <div id="<?php print $input_id; ?>-selected-container" class="selected-container"><i></i>
    <a href="" class="toggle">Toggle Options</a>
    <span class="clear-container">
      | <a href="" class="clear">Clear Selection</a>
    </span>
  </div>
  <div class="select-container">
    <?php print $icons; ?>
  </div>
</div>
