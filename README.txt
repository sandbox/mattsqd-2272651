
Description
-----------
This module provides CCK with a text input widget. The widget is specialized to
display a list of font icons and allow the user to choose one.

This module has the ability to be added to later or by end users now with new
versions of font icon libraries. Out of the gate it supports only fontawesome
version 3.2.1. It is up to the module user to provide the necessary css to
display the font icons, this is simply for the widget editing.

Integrates with
---------------
Content Construction Kit (https://drupal.org/project/cck)

Installation
------------
1) Place this module directory in your "modules" folder (this will usually be
"sites/all/modules/"). Don't install your module in Drupal core's "modules"
folder, since that will cause problems and is bad practice in general. If
"sites/all/modules" doesn't exist yet, just create it.

2) Enable the module.

3) Find a CCK field that has a type of text and change it to use this widget.

Limitations
-----------
You must choose a version when you are setting up the widget. This is limited
to only fontawesome version 3.2.1. You are responsible for including the CSS
needed to display the icons.

Adding your own versions
-----------
These can be added by the maintainer or you can create them yourselves.

The first step is to implement hook_icon_font_widget_options(). The module
itself implements this hook to provide the version. See
icon_font_widget_icon_font_widget_options(). The key of each element will be
passed around to the templates for determining the display.

The next step is to provide the html that outputs all of your font icons. This
will be used by the widget so they can select an icon. You will need to
implement hook_icon_font_widget_icons(). This hook provides a single variable
of the version currently worked on. In your hook, ensure that the version you
want is the current, then return a string of html for your icons. See
icon-font-widget-fontawesome_3.2.1.tpl.php for an example of a string.

The third step is to make sure your html string for font icons is compatible
with icon-font-widget.js. If it is not, you'll need to preprocess
theme('icon_font_widget') and set a variable called $skip to any value for
JUST YOUR version. It is up to you to provide your own js file to process
your icons. A good starting place is copying icon-font-widget.js.

The last step is outputing your font icon. This module does not supply any
formatter for this. There is a theme('icon_font_widget_display') that you
can preprocess it and set the variable $icon that will be output to the page.

Author
------
AllPlayers.com
