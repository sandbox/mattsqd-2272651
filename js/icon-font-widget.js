Drupal.behaviors.icon_font_widget = function (context) {

  $('.icon-font-widget').not('.processed').addClass('processed').each(function () {
    // Allow user to override this version so that a different JS can do this.
    if ($(this).attr('data-skip').length > 0) return;

    // Initialize the local variables.
    var version = $(this).attr('data-version');
    var inputId = '#' + $(this).attr('data-input-id');
    var selectedSelector = '.selected-container';
    var selectSelector = '.select-container';
    var iconSelector = 'i';
    var context = $(this);

    // Keep track of the selected class
    var selectedClass = $(inputId).val();
    // We saved the selected class with the version prefixed so that when we
    // display the value of the widget we know what template to load.
    selectedClass = selectedClass.replace(version + '--', '');

    // Initialize the icon if we've already saved one
    if (selectedClass.length) {
      // Initialize the selected icon as selected.
      $(selectSelector + ' ' + iconSelector + '.' + selectedClass, context).attr('class', selectedClass + ' selected');
      // Hide the select box.
      $(selectSelector, context).hide();
      // Set the selected container to the current value.
      $(selectedSelector + ' ' + iconSelector, context).attr('class', selectedClass);
    } else {
      // Hide the clear link
      $(selectedSelector + ' span.clear-container', context).hide();
    }

    // Attach the events.
    // We've clicked on an icon to select it.
    $(selectSelector + ' ' + iconSelector, context).on('click', function () {
      // Retrieve this items original class.
      var iconClass = $(this).attr('data-class');
      // If we've already set a selected class and it's not the item we're on
      // un-select it.
      if (selectedClass.length && selectedClass != iconClass) {
        $(selectSelector + ' ' + iconSelector + '.' + selectedClass, context).attr('class', selectedClass);
      }
      // Keep track of the clicked class.
      selectedClass = iconClass;
      // Save the selected class to our hidden field so it's saved.
      $(inputId).val(version + '--' + iconClass);
      // Mark this item as selected.
      $(this).attr('class', iconClass + ' selected');
      // Set the class of the box that shows our selection as this new class.
      $(selectedSelector + ' ' + iconSelector, context).attr('class', iconClass);
      // Hide the box that lets them select icons.
      $(selectSelector, context).hide();
      // Show the clear link
      $(selectedSelector + ' span.clear-container', context).show();
    });

    // We've moused over an element that could be selected.
    $(selectSelector + ' ' + iconSelector, context).on('mouseover', function () {
      var iconClass = $(this).attr('data-class');
      // If this is the selected element, don't lose that class.
      if (iconClass == selectedClass) {
        iconClass += ' selected';
      }
      $(this).attr('class', iconClass + ' hovered');
    });

    // We've moused out of an element that could be selected.
    $(selectSelector + ' ' + iconSelector, context).on('mouseleave', function () {
      var iconClass = $(this).attr('data-class');
      // If this is the selected element, don't lose that class.
      if (iconClass == selectedClass) {
        iconClass += ' selected';
      }
      $(this).attr('class', iconClass);
    });

    // Toggle the the container that lets us select elements open and closed.
    $(selectedSelector + ' a.toggle', context).on('click', function (e) {
      $(selectSelector, context).toggle();
      e.preventDefault();
    });

    // Clear our current selection.
    $(selectedSelector + ' a.clear', context).on('click', function (e) {
      // Clear the hidden field.
      $(inputId).val('');
      // Hide the clear link
      $(selectedSelector + ' span.clear-container', context).hide();
      // Remove the icon from the selection icon.
      $(selectedSelector + ' ' + iconSelector, context).attr('class', '');
      // Show our select container
      $(selectSelector, context).show();
      // Hide our current selected item
      if (selectedClass.length) {
        $(selectSelector + ' ' + iconSelector + '.' + selectedClass, context).attr('class', selectedClass);
      }
      e.preventDefault();
    });
  });
}
